<?php
/**
 * User Model for operator login
 * 
 * @package		classes
 * @subpackage	operator.navi.models
 * @author gary wang (wangbaogang123@hotmail.com)
 */
 
class ModelUser extends Model
{
	/**
	 * Admin table for operator
	 */
	var $useradmin;
	
	/**
	 * Init table instance
	 */
	function __construct()
	{
		import("config.globalconfig");
		$global = Config::getConfig("global");
		$this->useradmin = Config::getTable($global->admintable);
		parent::__construct();
        
	}
	
	/**
	 * login check
	 */
	function getLogin($username)
	{
		$list = null;
		$sql = "SELECT * FROM `{$this->useradmin->getTableName()}` WHERE username = '{$username}'";

		if( ($result = $this->_db->query($sql)) )
			{
			if( ($row = $this->_db->fetchrow($result)) )
				{
				$list = $row;
				$list["total"] = $this->_db->numrows($result);
			}
		}

		$this->_db->freeresult($result);
		return $list;

	}
	
}

?>