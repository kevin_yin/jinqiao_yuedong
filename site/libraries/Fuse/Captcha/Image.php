<?php
/**
 * Captcha based on figlet text rendering service
 *
 * Note that this engine seems not to like numbers
 *
 * @category   Zend
 * @package    Zend_Captcha
 * @subpackage Adapter
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 * @version    $Id: Figlet.php 23775 2011-03-01 17:25:24Z ralph $
 */
class Fuse_Captcha_Image extends Zend_Captcha_Word
{

	/**
	 * Image width
	 *
	 * @var int
	 */
	protected $_width = 200;

	/**
	 * Image height
	 *
	 * @var int
	 */
	protected $_height = 50;

	/**
	 * Font size
	 *
	 * @var int
	 */
	protected $_fsize = 0;

	protected $_image = null;

	protected $_background_color = array(255, 255, 255);
	protected $_text_color = array(0, 0, 0);
	protected $_noise_color = array(100, 100, 100);

	protected $_font = "monofont.ttf";

	/**
	 * Constructor
	 *
	 * @param  null|string|array|Zend_Config $options
	 * @return void
	 */
	public function __construct($options = null)
	{
		parent::__construct($options);
		$this->_font = realpath(HOMEDIR."/site/includes/resource/".$this->_font);
	}
	/**
	 * Set captcha width
	 *
	 * @param  int $width
	 * @return Fuse_Captcha_Image
	 */
	public function setWidth($width)
	{
		$this->_width = $width;
		return $this;
	}

/**
     * Set captcha font size
     *
     * @param  int $fsize
     * @return Fuse_Captcha_Image
     */
    public function setFontSize($fsize)
    {
        $this->_fsize = $fsize;
        return $this;
    }

    /**
     * Set captcha image height
     *
     * @param  int $height
     * @return Fuse_Captcha_Image
     */
    public function setHeight($height)
    {
        $this->_height = $height;
        return $this;
    }

    public function setBackgroundColor($color = array())
    {
    	$this->_background_color = $color;
    }

    public function setTextColor($color = array())
    {
    	$this->_text_color = $color;
    }

    public function setNoiseColor($color = array())
    {
    	$this->_noise_color = $color;
    }

    public function getBackgroundColor()
    {
    	return $this->_background_color;
    }

    public function getTextColor()
    {
    	return $this->_text_color;
    }

    public function getNoiseColor()
    {
    	return $this->_noise_color;
    }

    public function setFont($font)
    {
    	$this->_font = $font;
    }

    public function getFont()
    {
    	return $this->_font;
    }

	/**
	 * Get font size
	 *
	 * @return int
	 */
	public function getFontSize()
	{
		return $this->_fsize;
	}

	/**
	 * Get captcha image height
	 *
	 * @return int
	 */
	public function getHeight()
	{
		return $this->_height;
	}

	/**
	 * Get captcha image width
	 *
	 * @return int
	 */
	public function getWidth()
	{
		return $this->_width;
	}

	/**
	 * Generate new captcha
	 *
	 * @return string
	 */
	public function generate()
	{
		$id = parent::generate();
		$this->_generateImage();
		return $id;
	}

	private function _generateImage()
	{
		$code = $this->getWord();
		/* font size will be 75% of the image height */
		if(!$this->_fsize){
			$this->_fsize = $this->_height * 0.75;
		}
		$this->_image = @imagecreate($this->_width, $this->_height) or die('Cannot initialize new GD image stream');
		/* set the colours */
		$background_color = imagecolorallocate($this->_image, $this->_background_color[0], $this->_background_color[1], $this->_background_color[2]);
		$text_color = imagecolorallocate($this->_image, $this->_text_color[0], $this->_text_color[1], $this->_text_color[2]);
		$noise_color = imagecolorallocate($this->_image, $this->_noise_color[0], $this->_noise_color[1], $this->_noise_color[2]);
		/* generate random dots in background */
		for( $i=0; $i<($this->_width*$this->_height)/3; $i++ ) {
			imagefilledellipse($this->_image, mt_rand(0,$this->_width), mt_rand(0,$this->_height), 1, 1, $noise_color);
		}
		/* generate random lines in background */
		for( $i=0; $i<($this->_width*$this->_height)/150; $i++ ) {
			imageline($this->_image, mt_rand(0,$this->_width), mt_rand(0,$this->_height), mt_rand(0,$this->_width), mt_rand(0,$this->_height), $noise_color);
		}
		/* create textbox and add text */
		$textbox = imagettfbbox($this->_fsize, 0, $this->_font, $code) or die('Error in imagettfbbox function');
		$x = ($this->_width - $textbox[4])/2;
		$y = ($this->_height - $textbox[5])/2;
		imagettftext($this->_image, $this->_fsize, 0, $x, $y, $text_color, $this->_font , $code) or die('Error in imagettftext function');
		/* output captcha image to browser */

	}

	/**
	 * Display the captcha
	 *
	 * @param Zend_View_Interface $view
	 * @param mixed $element
	 * @return string
	 */
	public function render(Zend_View_Interface $view = null, $element = null)
	{
		header('Content-Type: image/jpeg');
	    imagejpeg($this->_image);
		imagedestroy($this->_image);
		$session = new Zend_Session_Namespace('Zend_Form_Captcha_Id_');
		$session->captcha_id = $this->getId();
		return null;
	}

	public static function getSessionWord(){
		$session = new Zend_Session_Namespace('Zend_Form_Captcha_Id_');
		$id = $session->captcha_id;
		$session->unsetAll();
		$session = new Zend_Session_Namespace('Zend_Form_Captcha_'.$id);
		$word = $session->word;
		$session->unsetAll();
		return $word;
	}
}
