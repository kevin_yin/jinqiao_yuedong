<?php
 /**
 * Email Config for send email.
 * @package		includes.config
 * @author kevin (feng_yinqikai@163.com)
*/
class EmailConfig{

	var $host      = '{$host}';
	var $from 	   = '{$from}';
	var $fromemail = '{$fromemail}';
	var $fromname  = '{$fromname}';
	var $passwd    = '{$password}';
	var $charset   = 'UTF-8';
	var $altbody   = 'text/html';

}
?>