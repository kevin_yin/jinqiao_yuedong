<?php
include_once("gdextend.php");
include_once("imagethumb.php");
class ImageHelper{

	function savePost($pathname,$source="/upload/"){

		$source_root = dirname(Config_App::rootdir()).$source;
		$save_root = dirname(Config_App::rootdir())."/thumb/";

		$pathname_640 = $this->getPrefixImage($pathname,"640");
		$thumb = new ImageThumb($source_root.$pathname,640);
		$thumb->setSavePath($save_root.$pathname_640);
		$thumb->toThumb();

		
        $pathname_510 = $this->getPrefixImage($pathname,"510");
		$thumb = new ImageThumb($source_root.$pathname,510);
		$thumb->setSavePath($save_root.$pathname_510);
		$thumb->toThumb();
        
        $pathname_405 = $this->getPrefixImage($pathname,"405");
        $thumb = new ImageThumb($source_root.$pathname,405);
        $thumb->setSavePath($save_root.$pathname_405);
        $thumb->toThumb();        
    

		$pathname_200 = $this->getPrefixImage($pathname,"200");
		$thumb = new ImageThumb($save_root.$pathname_510,200);
		$thumb->setSavePath($save_root.$pathname_200);
		$thumb->toThumb();

		$pathname_100 = $this->getPrefixImage($pathname,"100");
		$thumb = new ImageThumb($save_root.$pathname_200,100);
		$thumb->setSavePath($save_root.$pathname_100);
		$thumb->toSliceThunb(100,100);

        /*
		$pathname_60 = $this->getPrefixImage($pathname,"60");
		$thumb = new ImageThumb($save_root.$pathname_100,60);
		$thumb->setSavePath($save_root.$pathname_60);
		$thumb->toSliceThunb(60,45);
        */

		return $pathname_200;

	}

	function getPrefixImage($image,$prefix)
	{
		$filelist = explode(".",$image);
		if(count($filelist)<2){
			return $image;
		}
		$index = count($filelist)-2;
		$filelist[$index] .= "_".$prefix;
		$filelist[count($filelist)-1] = "jpg";
		$pathname = implode(".",$filelist);
		return $pathname;

	}

}
?>