<?php
 /**
 * Privilege Config for operator
 * Created on 2010-6-24
 * @package		imag
 * @author gary wang (wangbaogang123@hotmail.com)
*/

 class PrivilegeConfig {

 	var $title = "网站管理系统";
 	var $CONTENT = "2";
 	var $QIANDAO = "3";
 	var $ADMIN_PRIVILEGE = "0";
 	var $LOGOUT          = "1";

 	function privilege(){

		return array(

			$this->CONTENT => array(
					"title"=>"预约列表",
					"p" => '0',
					"link"=>array(
						array(
							"title"=>"健康厨房",
							"link"=>"yuyue/list.php?type=1",
							"target"=>"mainFrame"
						),array(
							"title"=>"有氧站",
							"link"=>"yuyue/list.php?type=2",
							"target"=>"mainFrame"
						)
					)
			),
			$this->QIANDAO => array(
					"title"=>"签到列表",
					"p" => '0',
					"link"=>array(
						array(
							"title"=>"签到列表",
							"link"=>"qiandao/list.php",
							"target"=>"mainFrame"
						)
					)
			),
			
			$this->LOGOUT => array(
					"title"=>"退出",
					"p" => '0',
					"link"=>array(
						array(
							"title"=>"退出",
							"link"=>"navi/logout.php?forward=../",
							"target"=>"_top"
						)
					)
				)

		);
 	}
}
?>
