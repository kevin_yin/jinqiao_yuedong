<?php
/**
 * Fuse Model
 *
 * @category   Fuse
 * @package    Fuse_Model
 * @copyright  Copyright (c) 2010-now 75.cn (http://cms.e75.cn)
 * @author Gary wang(qq:465474550,msn:wangbaogang123@hotmail.com)
 *
 */
class Fuse_Model
{
	/**
     * Database source
     *
     * @var Zend_Db
     */
	protected $db;
	/**
     * Database charset
     *
     * @var string
     */
	protected $charset;


	/**
     * Constructor
     *
     * Initialize database instance and config it.
     *
     * @param  array                   $options
     * @return void
     */
	public function __construct( $options = array() )
	{
		foreach ($options as $key => $value) {
            $this->$key = $value;
        }

		if($this->db==null){
			if(empty($this->charset)){
				$this->charset="utf8";
			}
			if(empty($options)){
				$options = Config_Db::toArray($this->charset);
			}
        	$this->db = Zend_Db::factory('PDO_MYSQL', $options);
        }

	}


	/**
     * Quote string for sql safe.
     * @pa string                   $value
     * @return mixed
     */
	public function quote($value)
	{
		return $this->db->quote($value);
	}

	/**
     * QuoteInfo string for sql safe.
     * @param  string                   $sql
     * @param  string                   $value
     * @return string
     */
	public function quoteInto($sql,$value)
	{
		return $this->db->quoteInto($sql,$value);
	}

	/**
	 * QuoteList Array for sql safe.
	 * @param  array                   $list
     * @return array
	 */
	public function quoteList(array $list)
	{
		foreach($list as $key=>$value){
			if(is_string($value))
			{
				$list[$key] = $this->db->quote($value);
			}
		}
		return $list;
	}

	/**
     * Get Zend_Db instance.
     * @return Zend_Db
     */
	public function getDb()
	{
		return $this->db;
	}

	/**
	 * Store to table.
	 * @param  string                   $table
     * @param  array                    $row
     * @return string
	 */
	public function store($table, array $row)
	{
		$this->db->insert($table, $row);
		return $this->db->lastInsertId();
	}

	/**
	 * Update table.
	 * @param  string                   $table
     * @param  array                    $set
     * @param  string                   $where
     * @return int
	 */
	public function update($table, array $set, $where){
		return $this->db->update($table, $set, $where);
	}

	/**
	 * Delete from table.
	 * @param  string                   $table
     * @param  string                   $where
     * @return int
	 */
	public function delete($table, $where){
		return $this->db->delete($table, $where);

	}

	/**
	 * Query sql.
	 * @param  string                   $sql
     * @param  mixed                    $bind String or array
     * @return Zend_Db_Statement_Interface
	 */
	public function query($sql,$bind = array()){
		return $this->db->query($sql,$bind);
	}

	/**
	 * Get row.
	 * @param  string                   $sql
     * @param  mixed                    $bind String or array
     * @return array
	 */
	public function getRow($sql,$bind = array()){
		return $this->db->fetchRow($sql,$bind);
	}

	/**
	 * Get rowset.
	 * @param  string                   $sql
     * @param  mixed                    $bind String or array
     * @return array
	 */
	public function getRowSet($sql,$bind = array()){
		return $this->db->fetchAll($sql,$bind);
	}

}
?>
