<?php
/**
 * FilterInput is a class for filtering input from any data source
 *
 * @category   Fuse
 * @package    Fuse_Filter_Input
 * @copyright  Copyright (c) 2010-now 75.cn (http://cms.e75.cn)
 * @author Gary wang(qq:465474550,msn:wangbaogang123@hotmail.com)
 */
class Fuse_Email extends Zend_Mail
{
	public function __construct($smtp="", $transport_config = array())
	{
		parent::__construct("UTF-8");
//		if(empty($smtp)){
//			$smtp = Config_Email::$smtp;
//		}
//		if(empty($transport_config)){
//			$transport_config = Config_Email::$transport;
//		}
//		self::setDefaultTransport(new Zend_Mail_Transport_Smtp($smtp,$transport_config));
//		$this->setFrom(Config_Email::$from_email, Config_Email::$from_name);

		//读取配置文件
		if(class_exists("WebconfigHelper")!=true){
			include("WebConfigHelper.php");
		}
		$emailconfig     = WebconfigHelper::getemailconfig();
        //var_dump($emailconfig);
        //exit;

		if(empty($smtp)){
			$smtp = $emailconfig['smtpAddress'];
		}
		if(empty($transport_config)){
			$transport = array(
				'auth' => 'login',
				'username' => $emailconfig['username'],
				'password' => $emailconfig['emailPass']
			);
			$transport_config = $transport;
		}
		self::setDefaultTransport(new Zend_Mail_Transport_Smtp($smtp,$transport_config));
		$this->setFrom($emailconfig['email'], $emailconfig['senderName']);
	}

	public function sendto($subject,$content,$email,$email_name='',$fromname="",$type='html')
	{
		if(empty($email_name)){
			$email_name = $email;
		}
		$this->addTo($email, $email_name);
		$this->setSubject($subject);
		if($type=="html"){
			$this->setBodyHtml($content,'utf-8',Zend_Mime::ENCODING_BASE64);
		}else{
			$this->setBodyText($content,'utf-8',Zend_Mime::ENCODING_BASE64);
		}
		$this->send();

	}
}