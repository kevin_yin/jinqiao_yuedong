<?php
import('imag.component.controller');
import('imag.component.model');
import('imag.component.view');
import('imag.component.template');
import('imag.database.database');

import('imag.email.email');
import('Fuse.Cookie');

 /**
 * Created 2013-07-16 10:20:24
 *
 * @package		classes
 * @subpackage	yuedong
 */
class YuedongController extends Controller
{
	/**
	 * Constructor
	 */
	private $website = "yuedong";
	private $jinqiaodb = null;
	private $echo_type 	= 'json';		//
	

	function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask( 'project','project');
		$this->registerTask( 'qastore','qastore');
		$this->registerTask( 'getQaOption','getQaOption');
		$this->registerTask( 'cervix','cervix');
		$this->registerTask( 'dosendemail','dosendemail');
		$this->registerTask( 'sendmail','sendmail');
		$this->registerTask( 'userinfo','userinfo');
        $this->registerTask( 'share365','share365');
		
		$this->jinqiaodb = Config::getConfig("jinqiaodb");
        
	}
	
	/**
	 * 完整报告
	 */
	function cervix(){
		$id         = Request::getVar("id","get"); 
        
        //查看是否登录
        $uid        = Fuse_Cookie::getInstance()->yuedong_uid;
        if (empty($uid)){
            Response::redirect("./", '请先从首页登录！');
            exit;            
        }
        //end
		
		$model = $this->createModel("content",dirname( __FILE__ ));
		
		$qa_list = $this->getQaOption();
		$detail  = $model->getRow($id, $uid);
        
        //获取当前用户前一期和下一期的报告id
        $myReportIds = $this->getNearById($detail['id'], $uid);
		
		$score_total = $detail['score1'] + $detail['score2'] + $detail['score3'] + $detail['score4'] + $detail['score5'];
		
		//根据总分数获取对应的文字内容
		$score_desc = "";
		if($score_total >=13 && $score_total<=16){
			$score_array = array(
					'score1'=>$detail['score1'],
					'score2'=>$detail['score2'],
					'score3'=>$detail['score3'],
					'score4'=>$detail['score4'],
					'score5'=>$detail['score5']				
				);
			
			$str = array("a"=>3,"b"=>2,"c"=>4,"d"=>3,"e"=>2);
			asort($score_array);
			$first_score = "";
			foreach($score_array as $k=>$v){
				$first_score = $k;
				break;
			}
			
			$qa_id = 1;
			if($first_score == "score1"){
				$qa_id = 1;
			}elseif($first_score == "score2"){
				$qa_id = 2;
			}elseif($first_score == "score3"){
				$qa_id = 3;
			}elseif($first_score == "score4"){
				$qa_id = 4;
			}elseif($first_score == "score5"){
				$qa_id = 5;
			}
            
            //added by kimi 20130717 当分值为15分，且每个单项都为3分的时候，显示文字为inobody
            if ($score_total == 15 && 
                $score_array['score1'] == 3 && 
                $score_array['score2'] == 3 &&
                $score_array['score3'] == 3 &&
                $score_array['score4'] == 3 &&
                $score_array['score5'] == 3
                )
            {
                $qa_id = 1;    
            }    
            
            //added end
			
			$score_desc = $model->getDescRowSingle( $qa_id );
		}else{
			$score_desc = $model->getDescRow( $score_total );
		}

        //added by kimi  增加了pc端的模板
        $source = Request::getVar("source","get");
        if ($source == 'pc'){
            $tpl = 'full_report.html';
        }
        else{
            $tpl = 'ipad/cervix.html';
        }
        //added end
        		
		$view = $this->createView($tpl);
		$object = new stdClass();
		$object->uid = $uid;
		$object->id  = $id;
        $object->nextId  = $myReportIds['next_id'];
        $object->prevId  = $myReportIds['prev_id'];
        $object->homeurl  = config::homeurl();
		$object->score_total = $score_total;
		$object->timeformat  = substr($detail['created'],0,10);
		$object->score_desc = $score_desc;
		$view->assign($object);
		$view->display();
	}
	
	/**
	 * 提交测试报告
	 */
	public function qastore(){
		$answer_1 = Request::getVar("answer_1","post"); 
		$answer_2 = Request::getVar("answer_2","post"); 
		$answer_3 = Request::getVar("answer_3","post"); 
		$answer_4 = Request::getVar("answer_4","post"); 
		$answer_5 = Request::getVar("answer_5","post"); 
		$uid      = Request::getVar("uid","post"); 
		
   		if(empty($answer_1) || empty($answer_2) || empty($answer_3) || empty($answer_4) || empty($answer_5) || empty($uid)){
   			echo json_encode(array("result"=>"INVALID_INPUT"));
			exit;
   		}
   		
   		$object = new stdClass();
		$object->uid = $uid;
		$object->score1  = $answer_1;
		$object->score2  = $answer_2;
		$object->score3  = $answer_3;
		$object->score4  = $answer_4;
		$object->score5  = $answer_5;
		
		$model = $this->createModel("content",dirname( __FILE__ ));
		
		$id = $model->store($object, "sports_my_score");
		
		echo json_encode(array("result"=>"OK" ,"id"=>$id));
		exit;
   		
	}
	
	public function getQaOption(){
		//sports_test_options
		$model = $this->createModel("content",dirname( __FILE__ ));
		$itemlist = $model->getRowSet(" SELECT * FROM `sports_test_options` WHERE 1 ");
		
		$arr = array();
		foreach($itemlist as $k=>$v){
			$arr[$v['option_id']][$v['option_score1']] = $v['option_desc1'];
			$arr[$v['option_id']][$v['option_score2']] = $v['option_desc2'];
			$arr[$v['option_id']][$v['option_score3']] = $v['option_desc3'];
			$arr[$v['option_id']][$v['option_score4']] = $v['option_desc4'];
			$arr[$v['option_id']][$v['option_score5']] = $v['option_desc5'];
		}
		
		return $arr;
	}
	
	function project(){
        //查看是否登录
        $uid        = Fuse_Cookie::getInstance()->yuedong_uid;
        if (empty($uid)){
            Response::redirect("./", '请先从首页登录！');
            exit;            
        }
        //end
		
		$id = Request::getVar("id");
		$model = $this->createModel("content",dirname( __FILE__ ));
		
		$qa_list = $this->getQaOption();
		$detail  = $model->getRow($id, $uid);
		
		//$arr[$classid][$optionid];
		$answer_1 = $answer_2 =$answer_3=$answer_4 =$answer_5 = array();
		$answer_1['score'] = $detail['score1'];
		$answer_2['score'] = $detail['score2']; 
		$answer_2['desc']  = $qa_list[1][$detail['score2']]; 
		
		$answer_3['score'] = $detail['score3']; 
		$answer_3['desc']  = $qa_list[2][$detail['score3']]; 
		
		$answer_4['score'] = $detail['score4']; 
		$answer_4['desc']  = $qa_list[3][$detail['score4']]; 
				
		$answer_5['score'] = $detail['score5']; 
		$answer_5['desc']  = $qa_list[4][$detail['score5']]; 
		
		//added by kimi  增加了pc端的模板
        $source = Request::getVar("source","get");
        if ($source == 'pc'){
            $tpl = 'scores.html';
        }
        else{
            $tpl = 'ipad/project.html';
        }
        //added end
        
        $view = $this->createView($tpl);
		$object = new stdClass();
		$object->answer_1 = $answer_1;
		$object->answer_2 = $answer_2;
		$object->answer_3 = $answer_3;
		$object->answer_4 = $answer_4;
		$object->answer_5 = $answer_5;
		
		$object->uid = $uid;
		$object->id = $id;
		$view->assign($object);
		$view->display();
	}
	
	function imageCreateFromMime($sourcePath){
		list($width, $height, $mime) = getimagesize($sourcePath);
		switch($mime){
			case IMAGETYPE_GIF:
				return imagecreatefromgif($sourcePath);
				break;
			case IMAGETYPE_JPEG:
				return imagecreatefromjpeg($sourcePath);
				break;
			case IMAGETYPE_PNG:
				return imagecreatefrompng($sourcePath);
				break;
			case IMAGETYPE_BMP:
				//return $this->imagecreatefrombmp($sourcePath);
				return null;
				break;
		}
	}

	function utf8Substr($str, $from, $len) 
	{ 
		return preg_replace('#^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$from.'}'. 
		'((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$len.'}).*#s', 
		'$1',$str);
	} 

	/**
	 * 文字换行
	 */
	function autowrap($fontsize, $angle, $fontface, $string, $width) {
		// 这几个变量分别是 字体大小, 角度, 字体名称, 字符串, 预设宽度
		$content = "";
	
		// 将字符串拆分成一个个单字 保存到数组 letter 中
		//mb_strlen
		for ($i=0;$i<iconv_strlen($string);$i++) {
			//$letter[] = mb_substr($string, $i, 1, "utf-8");
			$letter[] = $this->utf8Substr($string, $i, 1);
		}
	
		foreach ($letter as $l) {
			$teststr = $content." ".$l;
			$testbox = imagettfbbox($fontsize, $angle, $fontface, $teststr);
			// 判断拼接后的字符串是否超过预设的宽度
			if (($testbox[2] > $width) && ($content !== "")) {
				$content .= "\n";
			}
			$content .= $l;
		}
		return $content;
	}

	/**
	 * 合成图片
	 */
	function imagecopy($id, $uid, $image_time, $image_total, $image_desc, $image_advice, $image_content){
		$homedir = Config::homedir();
		$homeurl = Config::homeurl();
		
		$bg_image = $homedir."/edm/images/index_01.png";
		 
		$im = $this->imageCreateFromMime($bg_image);
		
		$font_size = 12;
		$font_size_jianyi = 12;
		$textcolor = imagecolorallocate($im, 0, 0, 0);
		$textcolor_white = imagecolorallocate($im, 255, 255, 255);
		
		$font   = $homedir.'/simhei.ttf';
		
		//日期
		ImageTTFText($im,  $font_size, 0, 575, 74, $textcolor, $font, $image_time);
		
		//总分
		ImageTTFText($im,  40, 0, 65, 220, $textcolor_white, $font, $image_total);
		
		//内容1
		$content = $this->autowrap($font_size, 0, $font, $image_desc, 442); // 自动换行处理
		ImageTTFText($im,  $font_size, 0, 205, 186, $textcolor, $font, $content);
		
		//内容2
		$content = $this->autowrap($font_size, 0, $font, $image_advice, 442); // 自动换行处理
		ImageTTFText($im,  $font_size, 0, 70, 338, $textcolor, $font, $content);
		
		//建议
		$content = $this->autowrap($font_size_jianyi, 0, $font, $image_content, 442); // 自动换行处理
		ImageTTFText($im,  $font_size_jianyi, 0, 200, 485, $textcolor, $font, $content);
		
		$to_image_name = "yuedong_".$id."_".$uid."_".time().".jpg";
		
		$repost_root = $homedir."/combinimage/";
		imagejpeg($im,$repost_root.$to_image_name,100);
		return 	"combinimage/".$to_image_name;
	}	
	
	function dosendemail(){
		$id  = Request::getVar("id","post"); 
		$uid = Request::getVar("uid","post"); 
		
		if(empty($id) || empty($uid)){
			echo json_encode(array("result"=>"INVALID_INPUT"));
			exit;
		}

		$image_time = Request::getVar("image_time","post"); 
		$image_total = Request::getVar("image_total","post"); 
		$image_desc = Request::getVar("image_desc","post"); 
		$image_advice = Request::getVar("image_advice","post"); 
		$image_content = Request::getVar("image_content","post"); 
		
		$model = $this->createModel("content",dirname( __FILE__ ));
		
		//判断是否已经发过邮件了
		$checkemail = $model->checkSend($id, $uid);
		if($checkemail['total'] > 0){
			echo json_encode(array("result"=>"HAS_SEND"));
			exit;	
		}
		
		//$new_image = "combinimage/yuedong_7_36105_1373971756.jpg";
		//合成图片
		$new_image = $this->imagecopy($id, $uid, $image_time, $image_total, $image_desc, $image_advice, $image_content);
		
		//发送邮件
		$subject = "悦动有志体测报告";
        $objectsend = new stdClass();
        
        $jinqiaouser = $this->getJinqiaoUser($uid);
        $useremail = "";
        if(empty($jinqiaouser)){
        	echo json_encode(array("result"=>"FAIL"));
			exit;
        }else{
        	$useremail = $jinqiaouser['xf_vipemail'];
        }
        
        $objectsend->email = $useremail;//收件人
        $objectsend->subject = $subject;
        $objectsend->new_image = $new_image;
        $objectsend->website = $this->website;
        $this->sendemail($objectsend);
		
		//记录数据表
		$object = new stdClass();
		$object->projectid = $id;
		$object->uid  = $uid;
		$object->image  = $new_image;
		$object->content  = $image_content;
		$object->ip  = $_SERVER['REMOTE_ADDR'];
		
		$result = $model->store($object, "sports_mail_send");
		
		echo json_encode(array("result"=>"OK"));
		exit;
	}
	
   /**
    *  邮件发送
    */
    function sendemail($vars){
        $emailer = new Email();
        $object = new stdClass();
        $object->host = "mail2.jinqiaojinqiao.com";
        $object->from = "event";
        $object->fromname = "金桥国际";
        $object->passwd = "2wsx1qaz";
        $object->fromemail  = $vars->email;
        $object->charset = "utf-8";
        $object->altbody  = "";
        $emailer->setConfig($object);
        $emailer->From = "mail2.jinqiaojinqiao.com";
		
        $view  = $this->createView("ipad/email.html");
        $view->assign($vars);
        $body = $view->fetch();

        return $emailer->sendEmail($vars->email,$vars->subject, $body);
    }
    
    function userinfo(){
    	$jinqiaodb = $this->getjinqiaodb();	
    	
        $sql = "SELECT  vid, xf_surname, xf_givenname, xf_vipcode, xf_telephone, xf_vipemail, xf_vipid FROM user_vip WHERE xf_vipcode = '100004399' ";
    	
    	$userinfo = $jinqiaodb->getjinqiaousers($sql);
    	
    	var_dump(iconv('GBk', 'UTF-8', $userinfo['xf_givenname']));
    }
    
    function getJinqiaoUser($vid){
    	$list = null;
        
        $this->openDB();
        
        $sql = "SELECT  vid, xf_surname, xf_givenname, xf_vipcode, xf_telephone, xf_vipemail, xf_vipid FROM user_vip WHERE vid = '{$vid}' ";
         if( ($result = $this->db->query($sql)) ){
            if( ($row = $this->db->fetchrow($result))){
                $list = $row;
            }
        }
        
        $this->db->freeresult($result);
        return $list;	
    }
    
    function openDB()
    {
        include("mysql.php");        
        $host       = 'localhost';
        $user       = 'root';
        $password   = 'say1171i';
        $db         = 'jinqiao';
        $arr        = array("host"=>$host, "user"=>$user, "password"=>$password, "database"=>$db);
        $this->db         = new MysqlLink($arr);
        $this->db->query(" set names gbk ");     
    } 
    
    function getjinqiaodb(){
        define("DBUTF8",false);
 		$jinqiao = $this->createModel("content",dirname( __FILE__ ),array("dbo"=>Factory::getDb($this->jinqiaodb->getOption())));
    	return $jinqiao;
    }
    
    
    
    /**
    * 获取当前报告的前一期和后一期的id
    * 
    * @param mixed $news_id
    * @param mixed $created
    * @param mixed $news_cate_id
    */
    function getNearById($id, $uid)
    {
        $model  = $this->createModel('content', dirname(__FILE__));        
        $_array = array();
        $where  = " `uid` = '{$uid}' ";
        $order  = ' ORDER BY created DESC, id DESC ';  
        $limit  = null;
        $itemList   =  $model->getList1($where, $order, $limit); 
        foreach($itemList as $key=>$value)
        {
            if ($value['id'] == $id)
            {
                if ($key == 0)
                {
                    $_array['prev_id'] = 0;
                }
                else
                {
                    $_array['prev_id'] = $itemList[$key-1]['id']; 
                }
                
                if ($key == count($itemList) -1)
                {
                    $_array['next_id'] = 0;
                }
                else
                {
                    $_array['next_id'] = $itemList[$key+1]['id']; 
                }                  
            }
        }
        
        return $_array;
    } 
    
    
    
    /**
    * 分享到365社区   
    */
    function share365()
    {
        $uid        = Fuse_Cookie::getInstance()->uid;
        $id         = Request::getVar("id","post"); 
        
        if(empty($id) || empty($uid)){
            echo json_encode(array("result"=>"INVALID_INPUT"));
            exit;
        }

        $image_time = Request::getVar("image_time","post"); 
        $image_total = Request::getVar("image_total","post"); 
        $image_desc = Request::getVar("image_desc","post"); 
        $image_advice = Request::getVar("image_advice","post"); 
        $image_content = Request::getVar("image_content","post");  
        
        $model = $this->createModel("content",dirname( __FILE__ ));
        
        $new_image = $this->imagecopy($id, $uid, $image_time, $image_total, $image_desc, $image_advice, $image_content); 
        
        //
        
        //发布帖子
        
                      
        
        
    }
    
    
}
?>