<?php
import('operator.navi.admincontroller');

 /**
 * Created on 2010-10-28
 * 
 * @package		classes
 * @subpackage	operator.global
 * @author gary wang (wangbaogang123@hotmail.com)
 */
class BasicController extends AdminController
{
	/**
	 * Constructor
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
		
		$privilegeConfig = $this->getPrivilegeConfig();
		if(!($this->checkPrivilege($privilegeConfig->BASIC))){
			$this->redirect("/operator/right.php","您没有权限访问该页面！");
		}
		
		$this->registerTask( 'search','jquerySearchList');
		$this->registerTask( 'list','itemList');
		
		$this->registerTask( 'insert','insert');
		$this->registerTask( 'modify','modify');
		$this->registerTask( 'update','update');
		$this->registerTask( 'delete','delete');
		
		$this->registerTask( 'input','input');

	}
	
	/**
	 * jquery 搜索
	 */
	function jquerySearchList()
	{
		import('operator.navi.tool');
		$key = Request::getVar("key",'post');
		$p = Request::getVar('p','get');
       	$value = Tool::unicode_urldecode(Request::getVar("value",'post'));

		$model = $this->createModel("basic",dirname( __FILE__ ));
		$where = "";
		if($value){
			$where = "title like '%$value%'";
		}
		if(empty($p)){$p=1;}
		$baseurl = "list.php?key=".$key."&value=".Request::getVar("value",'post');
		$perpage = 20;
		
		$totalitems = $model->getTotal($where);
	
		$style = new PageStyle();
		
		$options = array(
			"baseurl"	 => baseurl,
			"totalitems" => $totalitems,
			"perpage"	 => $perpage,
			"page"	     => $p,
			"maxpage"	 => 15,
			"pagestyle"  => $style,
			"showtotal"  => false
		);
		$pagination = new Pagination($options);
		$p = $pagination->getPage();
		
		$start = ($p-1)*$perpage;
		$itemList = $model->getList($start,$perpage,$where);
		
		$view  = $this->createView("operator/basic/list.html");
		$object = new stdClass();
		$object->PAGINATION = $pagination->fetch();
		$object->itemList = $itemList;
		$view->assign($object);
		$view->display();
	}
	
	/**
	 * 普通列表
	 */
	 function itemList()
	 {
		$p = Request::getVar('p','get');

		$model = $this->createModel("basic",dirname( __FILE__ ));
	
		if(empty($p)){$p=1;}
		$baseurl = "list.php";
		$perpage = 20;
		
		$totalitems = $model->getTotal();
	
		$style = new PageStyle();
		$options = array(
			"baseurl"	 => $baseurl,
			"totalitems" => $totalitems,
			"perpage"	 => $perpage,
			"page"	     => $p,
			"maxpage"	 => 15,
			"pagestyle"  => $style,
			"showtotal"  => false
		);
		$pagination = new Pagination($options);
		$p = $pagination->getPage();
		
		$start = ($p-1)*$perpage;
		$itemList = $model->getList($start,$perpage);
		
		$view  = $this->createView("operator/basic/list.html");
		$object = new stdClass();
		$object->PAGINATION = $pagination->fetch();
		$object->itemList = $itemList;
		$view->assign($object);
		$view->display();
	}
	
	/**
	 * Create html
	 */
	function create(){
		
		$model = $this->createModel("basic",dirname( __FILE__ ));
		$Fckeditor = $this->getFckEditor("content", "");
		$view  = $this->createView("operator/basic/create.html");
		$object = new stdClass();
		$object->title = "Tilte";
		$object->getFckeditor = $Fckeditor;
		$object->task = "insert";
		$view->assign($object);
		$view->display();
		
	}
	
	/**
	 * insert
	 */
	function insert(){
		$forward = Request::getVar("forward");
		if(empty($forward))
		{
			$forward = "list.php";
		}
	
		$title = Request::getVar("title","post");
		$content = Request::getVar("content","post",'',REQUEST_ALLOWRAW);
		
		if(empty($title)||empty($content)){
			$this->redirect($forward,"缺少参数！");
		}
		
		$model = $this->createModel("basic",dirname( __FILE__ ));

		$object = new stdClass();
		$object->title = $title;
		$object->content = $content;
		$object->ip   = $_SERVER['REMOTE_ADDR'];
		$object->time = gmdate('Y-m-d H:i:s');
		
		if ($model->store($object)){
			$this->redirect($forward,"添加成功");
		}else{
			$this->redirect($forward,"添加失败");
		}
	}
	
	/**
	 * modify
	 */
	function modify(){
		
		$id = Request::getVar('id','get');
		$forward = Request::getVar("forward");
		if(empty($id)){
			$this->redirect($forward,"缺少参数！");
		}
		$model = $this->createModel("basic",dirname( __FILE__ ));
		$row = $model->getRow($id);

		$Fckeditor = $this->getFckEditor("content", $row['content']);		

		$view  = $this->createView("operator/basic/modify.html");
		$object = new stdClass();
		$object->getFckeditor = $Fckeditor;
		$object->row = $row;
		$object->title = "Title";
		$view->assign($object);
		$view->display();
		
	}
	
	/**
	 * update
	 */
	function update(){
		
		$forward = Request::getVar("forward");
		if(empty($forward)){
			$forward = Request::getVar("HTTP_REFERER",'server');
		}

		$id = Request::getVar('id','post');
		$title = Request::getVar("title","post");
		$content = Request::getVar("content","post",'',REQUEST_ALLOWRAW);

		if(empty($id)){
			$this->redirect($forward,"id不能为空!");
		}
		
		if(empty($title)||empty($content)){
			$this->redirect($forward,"缺少参数！");
		}

		$model = $this->createModel("basic",dirname( __FILE__ ));
		
		$object = new stdClass();
		$object->title = $title;
		$object->content = $content;
		$object->nid = $id;
		
		if ($model->update($object,'nid')){
			$this->redirect("list.php","修改成功!");
		}else{
			$this->redirect($forward,"修改失败!");
		}
	}
	
	/**
	 * delete one
	 */
	function delete(){
		$forward = Request::getVar("forward");
		if(empty($forward)){
			$forward = "list.php";
		}
		$id = Request::getVar('id','get');
		
		if(empty($id)){
			$this->redirect($forward,"id不能为空!");
		}
		
		$model = $this->createModel("basic",dirname( __FILE__ ));
		
		if ($model->delete($id)){
			$this->redirect($forward,"删除成功");
		}else{
			$this->redirect($forward,"删除失败");
		}
	}
	
	/**
	 * input test
	 */
	function input(){
		
		$forward = Request::getVar("forward");
		if(empty($forward)){
			$forward = Request::getVar("HTTP_REFERER",'server');
		}

		$var_list = array(
			'email'=>'email',
			'username'=>'username',
			'password'=>'password',
			'mobile'=>'mobile',
			'question'=>'question',
			'answer'=>'answer'
		);
		
		$object =  $this->_getVarFromPost($var_list,$_POST);
		
		if($object==null)
		{
			$this->redirect($forward,"表单填写不完整！");
		}

		$object->password = md5($object->password);
		$object->msn = Request::getVar('msn','post');
		$object->qq = Request::getVar('qq','post');

		$model = $this->createModel("user",dirname( __FILE__ ));
		
		$object->ip   = $_SERVER['REMOTE_ADDR'];
		$object->time = gmdate('Y-m-d H:i:s');
		
		$result = $model->store($object,"c_user");

		if(!$result)
		{
			$this->redirect($forward,"表单提交失败！");
		}
		else
	    {	
			$this->redirect($forward,"表单提交成功！");
		}
		
	}
	
	
}
?>
