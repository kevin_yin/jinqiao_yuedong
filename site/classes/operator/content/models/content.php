<?php
/**
 * Example model
 * Created 2011-11-10 16:20:24
 * @package		classes
 * @subpackage	operator.Temp2011_what_content
 * @author gary wang (wangbaogang123@hotmail.com)
 *
 */
class ModelContent extends Model
{
	/**
	 * array
	 */
	var $table = array("name"=>"sports_reserve","key"=>"reserve_id");

	function __construct($config=array())
	{
		parent::__construct($config);
	}

	function getList($start=0,$per_page=10,$where=1)
	{
		$list = array();
		$sql = " SELECT * FROM `{$this->table['name']}` " .
				"WHERE {$where}   " .
				"ORDER BY {$this->table['key']} DESC";

		//var_dump($sql);

		if(!empty($per_page)){
			$sql .= " LIMIT {$start},{$per_page}";
		}

		if( ($result = $this->_db->query($sql)) )
		{
			while ( ($row = $this->_db->fetchrow($result)) )
			{
				$list[] = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;
	}

	function getTotal($where=1)
	{
		$total = array();

		$sql = "SELECT COUNT(*) AS total FROM `{$this->table['name']}` WHERE {$where} ";

		if(($result = $this->_db->query($sql)))
		{
			while(($row = $this->_db->fetchrow($result)))
			{
				$total = $row['total'];
			}
		}

		$this->_db->freeresult($result);
		return $total;
	}

	function getTotalAll($where=1)
	{
		$total = 0;

		$sql = "SELECT COUNT(*) AS total FROM `{$this->table['name']}` WHERE {$where}  ";

		if(($result = $this->_db->query($sql)))
		{
			while(($row = $this->_db->fetchrow($result)))
			{
				$total = $row['total'];
			}
		}

		$this->_db->freeresult($result);
		return $total;
	}

	function getRow($id)
	{
		$list = null;

		$sql = "SELECT * FROM `{$this->table['name']}` WHERE `{$this->table['key']}`='{$id}'";

		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$list = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;
	}

	function delete($id)
	{
		$sql = "DELETE FROM `{$this->table['name']}` WHERE `{$this->table['key']}`='{$id}'";
		return $this->_db->query($sql);
	}

	function getprizeinfo($id){
		$list = null;

		$sql = "SELECT * FROM `2012_year_gifts_list` WHERE `id`='{$id}'";

		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$list = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;
	}

	function getuserinfo($wuid){
		$list = null;

		$sql = "SELECT * FROM `2012_year_weibouser` WHERE `weibo_uid`='{$wuid}'";

		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$list = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;
	}
}
?>