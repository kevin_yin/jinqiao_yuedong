<?php

/**
 * Example model
 * Created 2011-11-10 16:20:24
 * @package		classes
 * @subpackage	operator.Temp2011_what_content
 * @author gary wang (wangbaogang123@hotmail.com)
 *
 */
import('imag.utilities.tool');

class ModelContent extends Model
{

	/**
	 * array
	 */
	var $table = array("name"=>"sports_my_score","key"=>"id");

	function __construct($config=array())
	{
		parent::__construct($config);
		$this->_db->query(" set names utf8 ");
	}

	function getlist($where=1)
	{
		$list = array();

		$sql = "SELECT * " .
				"FROM `{$this->table['name']}` " .
				"WHERE  $where " .
				"ORDER BY `time` DESC LIMIT 30";
		//var_dump($sql);
		if(($result = $this->_db->query($sql)))
		{
			while(($row = $this->_db->fetchrow($result)))
			{
				$list[] = $row;
			}
		}

		return $list;
	}
    
    
    function getList1($where, $order, $limit)
    {
        $list = array();
        $sql = "SELECT * FROM `{$this->table['name']}` WHERE {$where}";
        $sql .= " {$order} ";
        if ($limit != null)
        {
            $sql .= " LIMIT {$limit['start']},{$limit['offset']} ";    
        }
        //echo $sql;
        if( ($result = $this->_db->query($sql)) )
        {
            while ( ($row = $this->_db->fetchrow($result)) )
            {
                $list[] = $row;
            }
        }
        
        $this->_db->freeresult($result);
        return $list;
        
    }    

	function getRow($id, $uid)
	{
		$list = array();

		$sql = "SELECT * FROM `{$this->table['name']}` WHERE `uid`='{$uid}' ";
        
        //modofied by kimi 20130717 如果ID为空，则取最新一期
        if (empty($id)){
            $sql .= " ORDER BY `created` DESC LIMIT 0, 1 ";
        }
        else{
            $sql .= " AND `{$this->table['key']}`='{$id}'";
        }
        //end
        //echo $sql;

		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{                
                $list = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;
	}
	
	function getDescRow($score_total)
	{
		$list = array();

		$sql = "SELECT * FROM `sports_test_total` WHERE `min_score`<='{$score_total}' AND `max_score`>='{$score_total}'";

		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$list = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;
	}
	
	function getDescRowSingle( $qa_id ){
		$list = array();

		$sql = "SELECT * FROM `sports_test_single` WHERE `qa_id`='{$qa_id}' ";

		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$list = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;
	}
	
	function getRowCount($sql){
		$list = 0;

		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$list = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;
	}
	
	function checkSend($id, $uid){
		$list = 0;

		$sql = "SELECT count(*) as total FROM `sports_mail_send` WHERE `projectid`='{$id}' AND `uid`='{$uid}'";

		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$list = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;		
	}
	
	function checkValue($array,$table){

		$total = 0;
		if(empty($array)){
			$where = "1";
		}else{
			$where = implode(" AND ",$array);
		}
		$sql = "SELECT COUNT(*) AS total FROM `{$table}` WHERE {$where}";
		//var_dump($sql);

		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$total = $row['total'];
			}
		}

		$this->_db->freeresult($result);
		return $total;
	}
	
	function getjinqiaousers($sql){
		$list = "";

		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$list = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;	
	}
	
	function getStr($str,$length)
	{
		$oldlen = strlen($str);
		$str = tool::getstr($str,$length);
		if($oldlen>strlen($str))
		{
			$str .= "..";
		}
		return $str;
	}
}
?>
