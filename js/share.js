var Share = function ()
{
	var url     = "";
	var title   = "";
	var content_bk = "7.20 - 8.18，加入@金桥LOHAS #悦动有志#活力激发！体验健身体测、畅享悦动课程、炮制膳食计划，签到记录悦动轨迹，收获健康，赢取悦动达人神秘嘉奖！";
	var content = "7.20 - 8.18，加入@金桥LOHAS #悦动有志#活力激发！体验健身体测、畅享悦动课程、炮制膳食计划，签到记录悦动轨迹，收获健康，赢取悦动达人神秘嘉奖！";
	var pic = "http%3a%2f%2fdigihub.jinqiaojinqiao.com%2fyuedong%2fshare.jpg";
	var topic   = "";
	if(arguments.length==2){
		url   = arguments[0];
		title = arguments[1];
	}else if(arguments.length==3){
		url     = arguments[0];
		title   = arguments[1];
		content = arguments[2];
	}

	this.urlencode = function (){
		return encodeURIComponent(url);
	}

	this.url = function (){
		return encodeURIComponent(url);
	}
				
	this.title = function (){
		return title;
	}

	this.titleencode = function (){
		return encodeURIComponent(title);
	}

	this.pic = function (){
		return pic;
	}

	this.picencode = function (){
		return encodeURIComponent(pic);
	}
		
	this.contentencode = function (){
		return encodeURIComponent(content);
	}
		
	this.content = function (){
		return content;
	}
	
	this.setDefaultValue = function(picnew, wei_share_content){
		if(picnew){
			 pic = picnew;
		}

		if(wei_share_content){
			content = wei_share_content;
		}else{
			content = content_bk;
		}
	}

	this.kaixin = function (picnew, wei_share_content){	
		this.setDefaultValue(picnew, wei_share_content);
		window.open("http://www.kaixin001.com/rest/records.php?url="+ this.url() +"&content="+ this.contentencode()+"&pic="+pic+"&style=11");		
	}
	
	this.renren = function (picnew, wei_share_content){	
		this.setDefaultValue(picnew, wei_share_content);
		window.open("http://widget.renren.com/dialog/share?resourceUrl="+ this.url() +"&title="+ this.titleencode()+"&pic="+pic+"&description="+ this.contentencode());		
	}
			
	this.douban = function (picnew, wei_share_content){
		this.setDefaultValue(picnew, wei_share_content);
		window.open("http://www.douban.com/recommend/?url=" + this.url() + "&title="+ this.titleencode()+ "&rcontent=" + this.contentencode());
	}

	this.sina = function (picnew, wei_share_content){	
		this.setDefaultValue(picnew, wei_share_content);
		window.open("http://v.t.sina.com.cn/share/share.php?pic=" + pic + "&searchPic=false&url=" + this.url() + "&title="+topic+ this.contentencode());
	}

	this.qq = function (picnew, wei_share_content){	
		this.setDefaultValue(picnew, wei_share_content);
		window.open("http://share.v.t.qq.com/index.php?c=share&a=index&url="+ this.url() +"&title="+topic+ this.contentencode()+"&pic="+pic);
	}
}		