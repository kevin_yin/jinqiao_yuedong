<?php
class Config_App
{
	/**
	 * Current script scope directory root
	 */
	public static function basedir(){
		if(defined('BASEDIR')){
			return BASEDIR;
		}
		return dirname(dirname(__FILE__));

	}

	/**
	 * Root directory of this site
	 */
	public static function homedir(){
		if(defined('HOMEDIR')){
			return HOMEDIR;
		}
		return dirname(dirname(dirname(dirname(__FILE__))));
	}

	/**
	 * Root directory of this site
	 */
	public static function rootdir(){
		return dirname(dirname(dirname(dirname(__FILE__))));
	}

	/**
	 * Root web root of this site
	 */
	public static function homeurl(){
		return "http://".self::domain();
	}

	/**
	 * current domain
	 */
	public static function domain(){
		return $_SERVER['HTTP_HOST'];
	}

/**---------------------Cookie config----------------------------*/
	/**
	 * @return cookie domain
	 */
	public static function getCookieDomain()
	{
		return $_SERVER['HTTP_HOST'];
	}

	/**
	 * @return cookie expires
	 */
	public static function getCookieExpires()
	{
		return 0;
	}

	/**
	 * @return cookie path
	 */
	public static function getCookiePath()
	{
		return '/';
	}

	/**
	 * @return cookie secure
	 */
	public static function getCookieSecure()
	{
		return false;
	}

	/**
	 * @return cookie key
	 */
	public static function getCookieKey()
	{
		return "apllpsdsmmwqewqewqwennhyr!!@@#";
	}

	public static function getPrefixImage($image,$prefix)
	{
		$filelist = explode(".",$image);
		if(count($filelist)<2){
			return $image;
		}
		$index = count($filelist)-2;
		$filelist[$index] .= "_".$prefix;
		$filelist[count($filelist)-1] = "jpg";
		return implode(".",$filelist);
	}

	public static function getHead($image,$prefix){
		if(empty($image)){
			$image = "head.jpg";
		}
		return self::getPrefixImage($image,$prefix);
	}

	// 定义一个函数getIP()
	public static function getIP()
	{
		global $ip;
		if (getenv("HTTP_CLIENT_IP"))
		$ip = getenv("HTTP_CLIENT_IP");
		else if(getenv("HTTP_X_FORWARDED_FOR"))
		$ip = getenv("HTTP_X_FORWARDED_FOR");
		else if(getenv("REMOTE_ADDR"))
		$ip = getenv("REMOTE_ADDR");
		else $ip = "Unknow";
		return $ip;
	}
}
?>
