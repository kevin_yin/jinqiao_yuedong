<?php
 /**
 * Database JinqiaodbConfig
 * @package		includes.JinqiaodbConfig
 * @author gary wang (wangbaogang123@hotmail.com)
*/
class CampaigndbConfig{
	var $dbtype   = 'mysql';
	//var $host 	  = '192.168.0.5';
	var $host 	  = 'localhost'; //222.73.218.82
	var $user 	  = 'root';
	var $password = 'join_123456';
	var $db       = 'campaign';

	function getOption(){
		return array(
					"host" => $this->host,
					"user" => $this->user,
					"password" => $this->password,
					"database" => $this->db,
					"driver"   => $this->dbtype
		);
	}
}
?>
