<?php
 /**
 * Database Config
 * @package		includes.config
 * @author gary wang (wangbaogang123@hotmail.com)
*/
class DbConfig{

	var $dbtype   = 'mysql';
	var $host 	  = 'localhost';
	var $user 	  = 'root';
	var $password = 'say1171i';
	var $db       = 'jinqiao_home_center';

	function getOption(){
		return array(
					"host" => $this->host,
					"user" => $this->user,
					"password" => $this->password,
					"database" => $this->db,
					"driver"   => $this->dbtype
		);
	}

}
?>