<?php
/**
 * Example model
 * Created 2011-11-10 16:20:24
 * @package		classes
 * @subpackage	operator.Temp2011_what_content
 * @author gary wang (wangbaogang123@hotmail.com)
 *
 */
class ModelContent extends Model
{
	/**
	 * array
	 */
	var $table = array("name"=>"sports_qiandao","key"=>"qiandao_id");

	function __construct($config=array())
	{
		parent::__construct($config);
	}
    
	/**
	 * 导出报告	
	 */
	function getExportList($where=1, $jinqiaodb){
		$list = array();
		$sql = " SELECT `uid`, count(*) as total FROM `{$this->table['name']}` " .
				"WHERE {$where}   " .
				"group by `uid` ORDER BY total DESC ";

		if( ($result = $this->_db->query($sql)) )
		{
			while ( ($row = $this->_db->fetchrow($result)) )
			{
				$row['userinfo'] = $this->getUserInfo($row['uid'], $jinqiaodb);
				$list[] = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;
	}
	
	function getUserInfo($vid, $jinqiaodb){
		$list = null;

//		$sql = "SELECT * FROM `user` WHERE `vid`='{$vid}'";
//
//		if(($result = $this->_db->query($sql)))
//		{
//			if(($row = $this->_db->fetchrow($result)))
//			{
//				$list = $row;
//			}
//		}
//
//		$this->_db->freeresult($result);
		
		$sql = "SELECT  vid, xf_surname, xf_givenname, xf_vipcode, xf_telephone, xf_vipemail, xf_vipid FROM user_vip WHERE `vid` = '{$vid}' ";
		$result = $jinqiaodb->query($sql);
		$row = $jinqiaodb->fetchrow();
		$row['jinqiao_username'] = iconv('GBk', 'UTF-8', $row['xf_surname'].$row['xf_givenname']);
		return $row;	
	}
	
	function getList($start=0,$per_page=10,$where=1, $jinqiaodb)
	{
		$list = array();
		$sql = " SELECT * FROM `{$this->table['name']}` " .
				"WHERE {$where}   " .
				"ORDER BY {$this->table['key']} DESC";

		//var_dump($sql);

		if(!empty($per_page)){
			$sql .= " LIMIT {$start},{$per_page}";
		}

		if( ($result = $this->_db->query($sql)) )
		{
			while ( ($row = $this->_db->fetchrow($result)) )
			{
				$row['userinfo'] = $this->getUserInfo($row['uid'], $jinqiaodb);
				$list[] = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;
	}

	function getTotal($where=1)
	{
		$total = array();

		$sql = "SELECT COUNT(*) AS total FROM `{$this->table['name']}` WHERE {$where} ";

		if(($result = $this->_db->query($sql)))
		{
			while(($row = $this->_db->fetchrow($result)))
			{
				$total = $row['total'];
			}
		}

		$this->_db->freeresult($result);
		return $total;
	}

	function getTotalAll($where=1)
	{
		$total = 0;

		$sql = "SELECT COUNT(*) AS total FROM `{$this->table['name']}` WHERE {$where}  ";

		if(($result = $this->_db->query($sql)))
		{
			while(($row = $this->_db->fetchrow($result)))
			{
				$total = $row['total'];
			}
		}

		$this->_db->freeresult($result);
		return $total;
	}

	function getRow($id)
	{
		$list = null;

		$sql = "SELECT * FROM `{$this->table['name']}` WHERE `{$this->table['key']}`='{$id}'";

		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$list = $row;
			}
		}

		$this->_db->freeresult($result);
		return $list;
	}

	function delete($id)
	{
		$sql = "DELETE FROM `{$this->table['name']}` WHERE `{$this->table['key']}`='{$id}'";
		return $this->_db->query($sql);
	}
  
}
?>