<?php
import('imag.component.controller');
import('imag.component.model');
import('imag.component.view');
import('imag.component.template');
import('imag.database.database');

import('imag.environment.request');
import('imag.filesystem.filesystem');
import('imag.image.uploader');
import('imag.image.gdextend');
import('imag.image.imagecorp');
include_once("imagethumb.php");

class CropperController extends Controller
{
	function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask( 'cropper','cropper');
		$this->registerTask( 'upload','upload');
		$this->registerTask( 'box1','box1');
		$this->registerTask( 'box2','box2');
	}

	function box1(){
		
		$img = Request::getVar("img","get");
		$object = new stdClass();
		if(!empty($img)){
			$object->img = urldecode($img);
		}else{
			$object->img = "";
		}
		$object->type = "1";
		$object->isios = (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),"ipad") || strpos(strtolower($_SERVER['HTTP_USER_AGENT']),"iphone os"));
		$view  = $this->createView("box.html");
		$view->assign($object);
		$view->display();
	}

	function box2(){
		$img = Request::getVar("img","get");
		$object = new stdClass();
		if(!empty($img)){
			$object->img = urldecode($img);
		}else{
			$object->img = "";
		}
		$object->type = "2";
		$object->isios = (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),"ipad") || strpos(strtolower($_SERVER['HTTP_USER_AGENT']),"iphone os"));
		$view  = $this->createView("box.html");
		$view->assign($object);
		$view->display();
	}

	function upload()
	{
		$weibouid = 1;
		$upload_root = Config::homedir()."/temp/";

		$id = Request::getVar("id","get");

		if(array_key_exists('pic1',$_FILES) && $_FILES["pic1"]["error"] == UPLOAD_ERR_OK){
			try{
				$uploader = new Uploader($_FILES['pic1'],$upload_root);
				$uploader->setLevel(array(1,10));
				$uploader->setFileName($weibouid."_".time());
				$uploader->toUpload();
			}catch(Exception $e){
				echo "{result:'SIZE_LIMIT',id:'{$id}'}";
				exit();
			}
		}else{
			echo "{result:'FAIL',id:'{$id}'}";
			exit();
		}

		$object = new stdClass();
		$object->pathname = $uploader->getFolderFile();

		echo "{result:'OK',file:'{$object->pathname}',id:'{$id}'}";

	}

	/**
	 * Saves the record
	 */
	function cropper()
	{
		
		$options = new stdClass();
		$options->image  = Request::getVar("p","post");
		$options->width  = Request::getVar("w","post");
		$options->height = Request::getVar("h","post");
		$options->cropwidth  = Request::getVar("pw","post");
		$options->cropheight = Request::getVar("ph","post");

		if(Request::checkObject($options)){
			echo "{result:'INVALID_INPUT'}";
			exit();
		}

		$options->x      = Request::getVar("x","post");
		$options->y	     = Request::getVar("y","post");
		
		if(empty($options->x)){
			$options->x = 0;
		}
		if(empty($options->y)){
			$options->y = 0;
		}
		
		$savefile = strtolower($options->image);
		$savefile = str_replace("bmp", "jpg", $savefile);
		$sourcepath = Config::homedir()."/temp/".$options->image;
		$basepath   = $this->getPrefixImage($savefile,"1");
		$savepath   = Config::homedir()."/temp/{$basepath}";

		$cropper = new ImageCorp($sourcepath);
		$cropper->setSavePath($savepath);
		$cropper->toCorp($options,IMAGETYPE_JPEG);
		$cropper->destroy();
		
		FileSystem::delete(Config::homedir().$sourcepath);
		
		echo "{result:'OK',path:'{$basepath}'}";
		exit();
		
	}

	function getPrefixImage($image,$prefix)
	{
		$filelist = explode(".",$image);
		if(count($filelist)<2){
			return $image;
		}
		$index = count($filelist)-2;
		$filelist[$index] .= "_".$prefix;
		$filelist[count($filelist)-1] = "jpg";
		return implode(".",$filelist);
	}
}
?>