<?php
import('operator.navi.admincontroller');
import('imag.utilities.pagestyle');
import('imag.utilities.pagination');
import('imag.filesystem.fusefile');
import('imag.filesystem.filesystem');
import('imag.image.uploader');

 /**
 * Created 2011-11-11 09:54:34
 *
 * @package		classes
 * @subpackage	operator.content
 * @author gary wang (wangbaogang123@hotmail.com)
 */
class QiandaoController extends AdminController
{
	private $config = null;
	private $jinqiaodb = null;
	
	/**
	 * Constructor
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
		if (!session_id()) session_start();

		$privilegeConfig = Config::getConfig('privilege');
		if(!($this->checkPrivilege($privilegeConfig->QIANDAO))){
			$this->redirect('/operator/right.php','您没有权限访问该页面！');
		}


		$this->registerTask( 'list','itemList');
		$this->registerTask( 'dataExport','dataExport');

		$this->registerTask( 'upfile','upfile');
		$this->registerTask( 'changeState','changeState');
		//$this->config = Config::getConfig('cdb')->getOption();
		
		$this->jinqiaodb = Config::getConfig("jinqiaodb");
	}

	/**
	 * export excel
	 */
	function dataExport(){
		require_once("Excel/Writer.php");
		$model = $this->createModel("content",dirname( __FILE__ ));
		
		//链接金桥数据库
		$jinqiaodb = $this->getjinqiaodb();	
		$jinqiaodb = $jinqiaodb->getDbo();
		
		$wheresql = " 1 ";
		$itemList = $model->getExportList($wheresql, $jinqiaodb);
//		var_dump($itemList);
//
//		exit;

		$workbook = new Spreadsheet_Excel_Writer();

        //sending HTTP headers
        $title = "yuedong_qiandao_".date("YmdHi",time());
        $workbook->send($title.".xls");

        $shedtTitle = "yuedong_qiandao";
		$worksheet =& $workbook->addWorksheet($shedtTitle);

		$worksheet->write(0, 0, iconv("UTF-8","GBK//IGNORE","姓名"));
		$worksheet->write(0, 1, iconv("UTF-8","GBK//IGNORE","卡号"));
		$worksheet->write(0, 2, iconv("UTF-8","GBK//IGNORE","签到次数"));
		$r=1;
		foreach($itemList  as $item)
        {
        	$worksheet->writeString($r, 0, iconv("UTF-8","GBK//IGNORE",$item["userinfo"]['jinqiao_username']));
        	$worksheet->writeString($r, 1, iconv("UTF-8","GBK//IGNORE",$item["userinfo"]['xf_vipcode']));
        	$worksheet->writeString($r, 2, iconv("UTF-8","GBK//IGNORE",$item["total"]));
			$r++;
		}

		$workbook->close();

	}

    function getjinqiaodb(){
        define("DBUTF8",false);
 		$jinqiao = $this->createModel("content",dirname( __FILE__ ),array("dbo"=>Factory::getDb($this->jinqiaodb->getOption())));
    	return $jinqiao;
    }
    
	/**
	 * 普通列表
	 */
	 function itemList()
	 {
		$p = Request::getVar('p','get');
		$model 		  = $this->createModel('content', dirname( __FILE__ ));

		if(empty($p)){$p=1;}
		$baseurl = "list.php";
		$perpage = 20;

		$wheresql = " 1 ";

		$totalitems = $model->getTotal($wheresql);
		//$totalitemsAll = $model->getTotalAll($wheresql);

		$style = new PageStyle();
		$options = array(
			"baseurl"	 => $baseurl,
			"totalitems" => $totalitems,
			"perpage"	 => $perpage,
			"page"	     => $p,
			"maxpage"	 => 15,
			"pagestyle"  => $style,
			"showtotal"  => false
		);
		$pagination = new Pagination($options);
		$p = $pagination->getPage();

		$start = ($p-1)*$perpage;
		
		//链接金桥数据库
		$jinqiaodb = $this->getjinqiaodb();	
		$jinqiaodb = $jinqiaodb->getDbo();
		$itemList = $model->getList($start, $perpage, $wheresql, $jinqiaodb);

		//var_dump($itemList);
		$view  = $this->createView("operator/qiandao/list.html");
		$object = new stdClass();
		$object->PAGINATION = $pagination->fetch();
		$object->itemList = $itemList;
		$object->forward = $baseurl;
		$object->title = "签到列表";
		$view->assign($object);
		$view->display();
	}

	function upfile()
	{
		$file = $_FILES['file'];

		$forward = Request::getVar('HTTP_REFERER','server');

		if ($file['error'] > 0) {
            Response::redirect($forward, $file['error']);
		}

		$fileName 	   = date('YmdHis');
		$localFileName = Config::homedir() . '/txt/' . $fileName . '.txt';
		if (!move_uploaded_file($file['tmp_name'], $localFileName)) {
			Response::redirect($forward, '上传失败，请重新上传！');
		}

		if (!file_exists($localFileName)) {
			Response::redirect($forward, '上传失败，请重新上传！');
		}

		$list = $this->read($localFileName);

		$line = 0;
		foreach ($list as $value) {
			$line++;
			if ($this->checkid($value)) {
				$this->insert($value, $line, $fileName);
			}
		}

		Response::redirect($forward, '完成！');

	}

	function read($file){
        $list = array();
        $handle = fopen($file, 'r');
        while (!feof($handle)) {
			/*方法一：读单选，适合每行一个TAB*/
			// array_push($list,fgets($handle, 4096));
			/*方法二：读多行*/
			$str = fgets($handle, 4096);
			$pos = strrpos($str, '/');
			if ($pos === false) {
				continue;
			}
			$list[] = substr($str, $pos + 1);
        }
        fclose($handle);
        return $list;
    }

	/**
	 * insert
	 */
	function insert($weiboUid, $line, $fileName)
	{
		$model 		  = $this->createModel('content', dirname( __FILE__ ));
		$object		  = new stdClass();
		$object->uid  = $weiboUid;
		$object->date = date('Y-m-d');

		if (!$model->store($object, 'weibo_import')){
			$this->filewrite(Config::homedir() . '/txt/' . $fileName . '-log.txt', '原始数据：' . $weiboUid . '	' . '；文本第' . $line . '行写入失败！');
		}
	}

	/**
	 *
	 */
	function changeState(){
		$forward = Request::getVar('forward', 'get');
        $ispublish   = Request::getVar('ispublish', 'get');
        $id      = Request::getVar('id', 'get');

        if(empty($forward)){
            $forward = Request::getVar("HTTP_REFERER",'server');
        }

        if(empty($id)){
            $this->redirect($forward,"参数ID丢失");
        }


        $ispublish = $ispublish == 0 ? 0 : 1;
        $showmsg   = $ispublish == 0 ? '已撤销审核' : '审核成功';

        $model = $this->createModel("content",dirname( __FILE__ ));

        $object            = new stdClass();
        $object->id  = $id;
        $object->pass     = $ispublish;

        if ($model->update($object, "id", "2012fly_gamedata")){
            $this->redirect($forward, $showmsg);
        }else{
            $this->redirect($forward, '修改失败!');
        }
	}

	function checkid($weiboUid)
	{
		$model 		  = $this->createModel('content', dirname( __FILE__ ));
		$result = $model->getTotal(" `uid` = '{$weiboUid}' ");
		if ($result < 1) {
			return true;
		} else {
			return false;
		}
	}

    function filewrite($file, $content)
	{
        if($fp=fopen($file, 'a')){
            fwrite($fp, $content);
            fclose($fp);
        }
    }

}

?>