<?php
import('imag.component.controller');
import('imag.component.model');
import('imag.component.view');
import('imag.component.template');
import('imag.environment.request');
import('imag.database.database');

import('operator.navi.privilegehelper');
import('operator.leftscript');

/**
 * 
 * Controller for navigation
 *
 * @package		classes
 * @subpackage	operator.navi
 * @author gary wang (wangbaogang123@hotmail.com)
 * 
 */

class NaviController extends Controller
{

	/**
	 * Constructor
	 *
	 * @params	array	Controller configuration array
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
        
		$this->registerTask( 'index','display');
		$this->registerTask( 'left','left');
		$this->registerTask( 'right','right');



	}

	/**
	 * index
	 */
	function display()
	{
		$uid = Request::getVar("a_uid","cookie");
		if(!empty($uid)){
			$this->_index();
		}else{
			$view  = $this->createView("operator/navi/login.html");
			$object = new stdClass();
			$object->S_LOGIN_ACTION  = "navi/login.php";
			$view->assign($object);
			$view->display();
		}
	}

	/**
	 * index
	 */
	function _index()
	{
        $uid = Request::getVar("a_uid","cookie");
    	$phelper = new PrivilegeHelper();
		$_privilege = explode(",",$phelper->getPrivilege($uid));
		if(count($_privilege)==0||empty($uid)){
			$this->redirect("/code/operator","您没有权限访问！");
		}
		$view  = $this->createView("operator/navi/index.html");
		
		$object = new stdClass();
		$object->LEFT  = "left.php";
		$object->RIGHT = "right.php";
		$object->TITLE = "网站管理系统";
		$view->assign($object);
		$view->display();
	}

	/**
	 * left
	 */
	function left()
	{
        $uid = Request::getVar("a_uid","cookie");
        $phelper = new PrivilegeHelper();
		$_privilege = explode(",",$phelper->getPrivilege($uid));
		if(count($_privilege)==0||empty($uid)){
			$this->redirect("/code/operator","您没有权限访问！");
		}
		$view  = $this->createView("operator/navi/left.html");
		$object = new stdClass();
		$object->SCRIPT=LeftScript::script()."<script type=\"text/javascript\" src=\"js/item.js\"></script>";
		$object->TITLE  = "网站管理系统";
		$view->assign($object);
		$view->display();
	}

	/**
	 * right
	 */
	function right( )
	{
		$view  = $this->createView("operator/navi/right.html");
		$view->display();
	}

}
