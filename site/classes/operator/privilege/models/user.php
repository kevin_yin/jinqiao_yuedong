<?php
class ModelUser extends Model
{
	var $useradmin = null;
	function __construct()
	{
		import("config.globalconfig");
		$global = Config::getConfig("global");
		$this->useradmin = Config::getTable($global->admintable);
		parent::__construct();
		
	}
	
	function store($data,$table=''){
		$table = "{$this->useradmin->getTableName()}";
		if(!$this->_db->insertObject($table,$data)){
			return false;
		}else{
			return $this->_db->insertid();
		}

	}

	function update($data,$id,$table=''){
		$table = "{$this->useradmin->getTableName()}";
		return $this->_db->updateObject($table,$data,$id);

	}

	function getUserList($start,$per_page)
	{
		$list = null;

		$sql = "SELECT * FROM `{$this->useradmin->getTableName()}` WHERE 1 LIMIT $start,$per_page";
		
		if( ($result = $this->_db->query($sql)) )
		{
			while ( ($row = $this->_db->fetchrow($result)) )
			{
				$list[] = $row;
			}
		}
		
		$this->_db->freeresult($result);
		return $list;
	}
	
	function getTotal()
	{
		$list = null;
        
		$sql = "SELECT COUNT(*) AS total FROM `{$this->useradmin->getTableName()}` WHERE 1";
		
		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$list = $row['total'];
			}
		}
		
		$this->_db->freeresult($result);
		return $list;
	}
	
	function getUserInfo($aid)
	{
		$list = null;
		
		$sql = "SELECT * FROM `{$this->useradmin->getTableName()}` WHERE `{$this->useradmin->getKeyName()}`='{$aid}'";
		
		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$list = $row;
			}
		}
		
		$this->_db->freeresult($result);
		return $list;
	}
	
    
	function delete($aid)
	{
		$sql = "DELETE FROM `{$this->useradmin->getTableName()}` WHERE `{$this->useradmin->getKeyName()}`='{$aid}'";
		
		return $this->_db->query($sql);
	}

	function getPrivilege($aid){

		$sql = "SELECT privilege FROM `{$this->useradmin->getTableName()}` WHERE `{$this->useradmin->getKeyName()}`='{$aid}'";

		if (!($result = $this->_db->query($sql))){
				return array();
		}
			
		if(!($row = $this->_db->fetchrow($result))){
			return array();
		}
		
		$this->_db->freeresult($result);
		return $row['privilege'];
	}
}
?>