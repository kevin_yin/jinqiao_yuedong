<?php
/**
 * Fuse pagenator 
 *
 * @category   Fuse
 * @package    Fuse_Paginator
 * @copyright  Copyright (c) 2010-now 75.cn (http://cms.e75.cn)
 * @author Gary wang(qq:465474550,msn:wangbaogang123@hotmail.com)
 * 
 */
class Fuse_Paginator
{
	/**
	 * total count
	 */
	public $count;
	/**
	 * current page
	 */
	public $page;
	/**
	 * per page item count
	 */
	public $itemCountPerPage;
	/**
	 * page range of page navigation
	 */
	public $pageRange;
	
	/**
     * Constructor
     *
     * Initialize paginator source.
     *
     * @param  int                   $count
     * @param  int            $page
     * @param  int                   $itemCountPerPage
     * @param  int                   $pageRange
     * @return void
     */
	public function __construct($count, $currentpage, $itemCountPerPage=20, $pageRange=10)
	{
		$this->count            = $count;
		$this->page             = $currentpage;
		$this->itemCountPerPage = $itemCountPerPage;
		$this->pageRange        = $pageRange;
		$this->checkPage();
	}
	
	/**
	 * Check current page
	 */
	private function checkPage()
	{
		$total = ceil($this->count/$this->itemCountPerPage);
		if($this->page > $total)
		{
			$this->page = $total;
		}
		if(empty($this->page) || $this->page < 1)
		{
			$this->page = 1;
		}
	}
	
	/**
	 * Get pages
	 * @return array
	 */
	public function getPages()
	{
        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Null($this->count));
        $paginator->setItemCountPerPage($this->itemCountPerPage)
                  ->setCurrentPageNumber($this->page)
                  ->setPageRange($this->pageRange);
        return $paginator->getPages();
	}
	
	/**
	 * Get limit for sql query
	 * @return array
	 */
	public function getLimit()
	{
		return array(
					"start"=>($this->page-1)*$this->itemCountPerPage,
					"offset"=>$this->itemCountPerPage
		);
	}
	
}
?>
