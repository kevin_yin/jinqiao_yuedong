$(function(){
	var pathname = window.location.pathname;
	 
	//获取登录信息
	$.post("/yuedongapi/userinfo", {  },function(data){
		var item = eval("("+data+")");
		if(item.result == "OK"){
			$('#logincanvas').html('<div class="logoin_up"><span>你好，'+item.userinfo.nickname+'&nbsp;&nbsp;|&nbsp;</span><a href="javascript:logout_err();">&nbsp;退出</a></div>');
            $('#logincanvas').attr('loginstatus', 'y');
		}else{
			$('#logincanvas').html('<a href="login.html?forward='+pathname+'" class="member">会员登陆</a>');
            $('#logincanvas').attr('loginstatus', 'n');
		}
	});
 
})

function logout_err(){
	$.post("/yuedongapi/logoutajax/", {},function(data){
		var item = eval("("+data+")");
		if(item.result == "OK"){
			  alert("退出成功!");
			  window.location.href = "./";
		} 
	});
}

// 获取地址栏的参数数组
function getUrlParams()
{
    var search = window.location.search ; 
    // 写入数据字典
    var tmparray = search.substr(1,search.length).split("&");
    var paramsArray = new Array; 
    if( tmparray != null)
    {
        for(var i = 0;i<tmparray.length;i++)
        {
            var reg = /[=|^==]/;    // 用=进行拆分，但不包括==
            var set1 = tmparray[i].replace(reg,'&');
            var tmpStr2 = set1.split('&');
            var array = new Array ; 
            array[tmpStr2[0]] = tmpStr2[1] ; 
            paramsArray.push(array);
        }
    }
    // 将参数数组进行返回
    return paramsArray ;     
}

// 根据参数名称获取参数值
function getParamValue(name)
{
    var paramsArray = getUrlParams();
    if(paramsArray != null)
    {
        for(var i = 0 ; i < paramsArray.length ; i ++ )
        {
            for(var  j in paramsArray[i] )
            {
                if( j == name )
                {
                    return paramsArray[i][j] ; 
                }
            }
        }
    }
    return null ; 
}


